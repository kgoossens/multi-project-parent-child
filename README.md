Demo GitLab's multi-project pipelines (cross-project or interconnected projects) and parent-child pipelines in one project.

The purpose of this project is to help explain, and demo, the differences between a mulit-project pipeline (cross-project) and a Parnt-Child pipeline.

**Multi-Projec Pipelines**
> Pipelines for different projects can be combined and visualized together.
> https://docs.gitlab.com/ee/ci/multi_project_pipelines.html

**Parent-Child Pipelines**
> Complex pipelines can be broken down into one parent pipeline that can trigger multiple child sub-pipelines, which all run in the same project and with the same SHA.

> https://docs.gitlab.com/ee/ci/parent_child_pipelines.html



In the .gitlab-ci.yml the are three downstream Parent-Child pipeline .yml file for three microservices: microservice_a, microservice_b and microservice_c.  

There is also a mulit-project, in another repo, titled, "Simple Java Helloworld" pipeline: job2.
